import { Player } from "../models/Players/Player";

export interface IPlayersApi {
  retrieveAll(): Promise<Player[]>;
}
