import { IPlayersApi } from "./IPlayersAPI";
import { Player } from "../models/Players/Player";
import { nadal, zidane } from "../utils/fixtures/players.fixtures";

export class APIInMemoryPlayers implements IPlayersApi {
  retrieveAll = (): Promise<Player[]> => {
    return Promise.resolve([nadal, zidane]);
  };
}
