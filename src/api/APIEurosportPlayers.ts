import wretch from "wretch";
import { PlayerApiModel } from "./PlayersApiModel";
import { Player } from "../models/Players/Player";
import { PlayerBuilder } from "../models/Players/PlayerBuilder";
import { IPlayersApi } from "./IPlayersAPI";

export interface AllPlayersResponse {
  players: PlayerApiModel[];
}

export class APIEurosportPlayers implements IPlayersApi {
  public retrieveAll = (): Promise<Player[]> => {
    return wretch(
      "https://eurosportdigital.github.io/eurosport-web-developer-recruitment/headtohead.json"
    )
      .get()
      .json<AllPlayersResponse>()
      .then(response => response.players.map(this.mapPlayer));
  };

  private mapPlayer = (player: PlayerApiModel): Player => {
    return new PlayerBuilder()
      .withFirstName(player.firstname)
      .withLastName(player.lastname)
      .withShortName(player.shortname)
      .withSex(player.sex)
      .withCountry(player.country)
      .withPicture(player.picture)
      .withData(player.data)
      .build();
  };
}
