export interface CountryDataApiModel {
  picture: string;
  code: string;
}

export interface PlayerDataApiModel {
  rank: number;
  points: number;
  weight: number;
  height: number;
  age: number;
  last: number[];
}

type GenderDTO = "M" | "F";

export interface PlayerApiModel {
  firstname: string;
  lastname: string;
  shortname: string;
  sex: GenderDTO;
  country: CountryDataApiModel;
  picture: string;
  data: PlayerDataApiModel;
}
