import React, { Component } from "react";
import { Provider } from "react-redux";
import { Route, Router } from "react-router";
import { createBrowserHistory } from "history";
import { store } from "./modules/root.store";
import { PlayersContainer } from "./pages/Players/Players.container";

const history = createBrowserHistory();

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router history={history}>
          <Route path="/" exact component={PlayersContainer} />
        </Router>
      </Provider>
    );
  }
}

export default App;
