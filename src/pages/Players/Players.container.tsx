import { compose } from "redux";
import { PlayersComponent } from "./Players.component";
import { allPlayersContainerFactory } from "../../modules/allPlayers/containers/allPlayersContainerFactory";

export const PlayersContainer = compose(allPlayersContainerFactory)(
  PlayersComponent
);
