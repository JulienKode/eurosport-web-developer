import React from "react";
import styled from "styled-components";
import { Player } from "../../../models/Players/Player";
import { Card } from "../../../components/Card/Card.component";
import { Title } from "../../../components/Title/Title.component";
import { ImageRounded } from "../../../components/ImageRounded/ImageRounded.component";
import { PlayerDataComponent } from "../PlayerData/PlayerData.component";
import { Last } from "../../../components/Last/Last.component";

export const PlayerComponent = ({ player }: { player: Player }) => {
  return (
    <PlayerContainer>
      <Card>
        <Title>{`${player.firstName} ${player.lastName}`}</Title>
        <ListContainer>
          <ImageRounded
            alt={`${player.lastName}.${player.firstName}`}
            src={player.picture}
          />
          <PlayerDataComponent {...player.data} />
        </ListContainer>
        <Last last={player.data.last} />
      </Card>
    </PlayerContainer>
  );
};

const PlayerContainer = styled.div`
margin: 10px 10px 10px 10px;
display: flex;
flex 0.4;
justify-content: center;
flex-direction: column;
`;

const ListContainer = styled.div`
  margin: 10px 10px 10px 10px;
  display: flex;
  justify-content: center;
  align-items: center;
`;
