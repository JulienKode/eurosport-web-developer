import React from "react";
import { PlayerComponent } from "./Player.component";
import { nadal } from "../../../utils/fixtures/players.fixtures";
import { render } from "react-testing-library";

describe("PlayerComponent component", () => {
  it("should match snapshot", () => {
    // Given
    const wrapper = render(<PlayerComponent player={nadal} />);

    // Expect
    expect(wrapper).toMatchSnapshot();
  });
});
