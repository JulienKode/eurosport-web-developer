import { EPlayersActionType } from "./allPlayers.actions";
import { call, put, getContext, takeEvery } from "redux-saga/effects";
import { IGetPlayersAction } from "./allPlayers.actions";
import { createAction } from "../../utils/redux";
import { PLAYERS_CONTEXT_KEY } from "../app.context";

export function* getAllPlayers(action: IGetPlayersAction) {
  try {
    const playersGateway = yield getContext(PLAYERS_CONTEXT_KEY);
    const list = yield call(playersGateway.retrieveAll);
    yield put(createAction(EPlayersActionType.GET_SUCCESS, { list }));
  } catch (error) {
    yield put(
      createAction(EPlayersActionType.GET_FAILURE, { message: error.message })
    );
  }
}

export function* allPlayers() {
  yield takeEvery(EPlayersActionType.GET, getAllPlayers);
}
