import { Player } from "../../../models/Players/Player";

export interface IAllPlayersPropsContainerProps {
  getAllPlayers: () => void;
  players: Player[];
}
