import { Dispatch } from "react";
import { Action } from "redux";
import { connect } from "react-redux";
import { IAppState } from "../../app.state";
import { EPlayersActionType } from "../allPlayers.actions";
import { createAction } from "../../../utils/redux";
import { selectAllPlayers } from "../allPlayers.selector";

const mapStateToProps = (state: IAppState) => ({
  players: selectAllPlayers(state)
});

const mapDispatchToProps = (dispatch: Dispatch<Action>) => ({
  getAllPlayers: () => dispatch(createAction(EPlayersActionType.GET))
});

export const allPlayersContainerFactory = connect(
  mapStateToProps,
  mapDispatchToProps
);
