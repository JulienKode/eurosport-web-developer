import { Player } from "../../models/Players/Player";

export interface IAllPlayersState {
  list: Player[];
  isLoading: boolean;
  error: string | null;
}
