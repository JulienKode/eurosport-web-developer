import { IAppState } from "../app.state";
import { Player } from "../../models/Players/Player";

export const selectAllPlayers = (state: IAppState): Player[] => {
  return state.players.list;
};
