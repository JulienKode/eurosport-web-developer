import { selectAllPlayers } from "./allPlayers.selector";
import { IAppState } from "../app.state";
import { rootReducer } from "../root.reducers";
import { nadal, zidane } from "../../utils/fixtures/players.fixtures";

describe("selectAllPlayers", () => {
  let state: IAppState;

  beforeEach(() => {
    state = rootReducer(undefined, { type: "INIT" });
  });

  it("should return all players in the state", () => {
    // Given
    state.players = {
      list: [nadal, zidane],
      isLoading: false,
      error: null
    };

    // When
    const result = selectAllPlayers(state);

    // Expect
    expect(result).toEqual([nadal, zidane]);
  });

  it("should return empty array when there are no players", () => {
    // Given
    state.players = {
      list: [],
      isLoading: false,
      error: null
    };

    // When
    const result = selectAllPlayers(state);

    // Expect
    expect(result).toEqual([]);
  });
});
