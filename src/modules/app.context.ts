import {IPlayersApi} from "../api/IPlayersAPI";

export const PLAYERS_CONTEXT_KEY = 'playersGateway';

export interface IAppContext {
    [PLAYERS_CONTEXT_KEY]: IPlayersApi;
}
