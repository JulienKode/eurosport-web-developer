import { IAllPlayersState } from "./allPlayers/allPlayers.state";

export interface IAppState {
  players: IAllPlayersState;
}
