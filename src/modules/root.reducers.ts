import { combineReducers } from "redux";
import { allPlayersReducer } from "./allPlayers/allPlayers.reducers";

export const rootReducer = combineReducers({
  players: allPlayersReducer
});
