import { all, fork } from "redux-saga/effects";
import { allPlayers } from "./allPlayers/allPlayers.sagas";

export const allCoreSagas = [fork(allPlayers)];

export function* rootSaga() {
  yield all(allCoreSagas);
}
